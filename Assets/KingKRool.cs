﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingKRool : MonoBehaviour
{
    public int hp = 0;
    public int vel;
    public int velDown;
    public float jumpHeight;
    public float doubleJumpHeight;
    public bool isGrounded;
    public bool doubleJump = false;
    public GameObject hitbox;
    public GameObject challenger;
    public bool lastInput;
    public string moveUp;
    public string moveDown;
    public string moveRight;
    public string moveLeft;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(moveDown))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -velDown);
        }


        if (Input.GetKey(moveLeft))
        {
            lastInput = true;
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            this.GetComponent<Animator>().SetFloat("Speed", vel);
            this.GetComponent<SpriteRenderer>().flipX = false;
        }
        else if (Input.GetKey(moveRight))
        {
            lastInput = false;
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            this.GetComponent<Animator>().SetFloat("Speed", vel);
            this.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }

        if (Input.GetKeyDown(moveUp))
        {
            if (isGrounded == true)
            {
                this.GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpHeight;
                doubleJump = true;
            }
            else
            {
                if (doubleJump)
                {
                    doubleJump = false;
                    this.GetComponent<Rigidbody2D>().velocity = Vector2.up * doubleJumpHeight;
                }
            }
        }
        if (this.GetComponent<Rigidbody2D>().velocity.y > 0.1)
        {
            this.GetComponent<Animator>().SetBool("Jumping", true);
        }
        if(this.GetComponent<Rigidbody2D>().velocity.y < 0.1){
            this.GetComponent<Animator>().SetBool("Jumping", false);
        }


        
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }

        if(other.gameObject.tag == "EndOfMap")
        {
            this.transform.position = new Vector2(-3f,3f);
        }
    }


    void attack()
    {
        if (lastInput==true) {
            hitbox.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x - 0.5f, this.GetComponent<Transform>().position.y);
        }
        if (lastInput==false)
        {
            hitbox.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x + 0.5f, this.GetComponent<Transform>().position.y);
        }
        //IEenumerator -> corrutinas
        hitbox.SetActive(true);

    }
    public void OnLanding()
    {
        this.GetComponent<Animator>().SetBool("Jumping", false);
    }
}